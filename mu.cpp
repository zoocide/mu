#if defined(_WIN32)
#include <windows.h>
#include <psapi.h>

#elif defined(__unix__) || defined(__unix) || defined(unix) || (defined(__APPLE__) && defined(__MACH__))
#include <unistd.h>
#include <sys/resource.h>

#if defined(__APPLE__) && defined(__MACH__)
#include <mach/mach.h>

#elif (defined(_AIX) || defined(__TOS__AIX__)) || (defined(__sun__) || defined(__sun) || defined(sun) && (defined(__SVR4) || defined(__svr4__)))
#include <fcntl.h>
#include <procfs.h>

#elif defined(__linux__) || defined(__linux) || defined(linux) || defined(__gnu_linux__)
#include <stdio.h>

#endif //defined(__APPLE__) && defined(__MACH__)

#else //_WIN32
#error "Cannot detect memory usage for an unknown OS."
#endif //_WIN32

#include <iostream>

/**
 * Returns the peak (maximum so far) resident set size (physical memory use)
 * measured in bytes, or zero if the value cannot be determined on this OS.
 */
size_t getPeakRSS()
{
#if defined(_WIN32)
  PROCESS_MEMORY_COUNTERS pmc;
  GetProcessMemoryInfo(GetCurrentProcess(), &pmc, sizeof(pmc));
  return (size_t)pmc.PeakWorkingSetSize;

#elif (defined(_AIX) || defined(__TOS__AIX__)) || (defined(__sun__) || defined(__sun) || defined(sun) && (defined(__SVR4) || defined(__svr4__)))
  struct psinfo psi;
  int fd = -1;
  if ((fd = open("/proc/self/psinfo", O_RDONLY)) == -1)
    return 0;
  if (read(fd, &psi, sizeof(psi)) != sizeof(psi)) {
    close(fd);
    return 0;
  }
  close(fd);
  return (size_t)(psi.pr_rssize * 1024);

#elif defined(__unix__) || defined(__unix) || defined(unix) || (defined(__APPLE__) && defined(__MACH__))
  struct rusage ru;
  getrusage(RUSAGE_SELF, &ru);
#if defined(__APPLE__) && defined(__MACH__)
  return (size_t)ru.ru_maxrss;
#else
  return (size_t)(ru.ru_maxrss * 1024);
#endif

#else //unknown OS
  return 0;
#endif
}

/**
 * Returns the current resident set size (physical memory use) measured in
 * bytes, or zero if the value cannot be determined on this OS.
 */
size_t getCurrentRSS()
{
#if defined(_WIN32)
  PROCESS_MEMORY_COUNTERS pmc;
  GetProcessMemoryInfo(GetCurrentProcess(), &pmc, sizeof(pmc));
  return (size_t)pmc.WorkingSetSize;

#elif defined(__APPLE__) && defined(__MACH__)
  struct mach_task_basic_info info;
  mach_msg_type_number_t ic = MACH_TASK_BASIC_INFO_COUNT;
  if (task_info(mach_task_self(), MACH_TASK_BASIC_INFO, (task_info_t)&info, &ic) != KERN_SUCCESS)
    return 0;
  return (size_t)info.resident_size;

#elif defined(__linux__) || defined(__linux) || defined(linux) || defined(__gnu_linux__)
  long rss = 0;
  FILE* fp;
  if ((fp = fopen("/proc/self/statm", "r")) == NULL)
    return 0;
  if (fscanf(fp, "%*s%ld", &rss) != 1) {
    fclose(fp);
    return 0;
  }
  fclose(fp);
  return (size_t)rss * sysconf(_SC_PAGESIZE);

#else //unknown OS
  return 0;
#endif
}
