// v1.1.0
#ifndef FILE_MU__MU_H_
#define FILE_MU__MU_H_

size_t getPeakRSS();
size_t getCurrentRSS();

#endif //FILE_MU__MU_H_
