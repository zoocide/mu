#include "time.h"
#ifdef USE_SHARED_MU
#include "mu.h"
#else //USE_SHARED_MU
#include "mu.cpp"
#endif //USE_SHARED_MU

class PrintMem
{
public:
  PrintMem()
  {
    m_start_time = get_time();
  }
  void print_info()
  {
    std::cout << "peak memory usage = " << getPeakRSS() / (1024 * 1024) << "MB" << std::endl;
    std::cout << "execution time    = " << (get_time() - m_start_time) << std::endl;
  }
  ~PrintMem()
  {
    print_info();
  }
private:
  double m_start_time;
};

static PrintMem mem;
